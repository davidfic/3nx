
import sys
from matplotlib import pyplot as plt

number = 0

# check and update our largest number
def check_largest(largest_number, num):
    # compare curent number to largest number
    if num > largest_number:
        # if the new number is bigger make it the largest
        return num
    # return the current largest number
    return largest_number

# check if any of the numbers are prime
def is_prime(num):
    #set our initial finding to false
    flag = False


    # first make sure the number is positive
    if num > 1:
        # check for factors, we're starting at 2 and going all the way up 
        # to the value of the num variable we pass in
        for i in range(2, num):

            if (num % i) == 0:
                # if factor is found, set flag to True
                flag = True
                # we found a factor so it's not prime. break out of loop
                break
    
        # check if flag is True. If it's true that means there is a factor and 
        # not prime. If no factor was found that means it's prime
        if flag:
           return False 
        else:
            return True

# using matplotlib plot out the steps
def plot_numbers(numbers,primes):

    #this was mostly copied from their docs
    # setting up a plot with the numbers we supply it. 
    fig, (ax1,ax2) = plt.subplots(2)
    fig.suptitle('number and prime graph')


    # this plots the numbers we enountered and colors it red
    plt.subplot(121)
    plt.plot(numbers, color="red")
    plt.xlabel("numbers")

    # now we plot the primes and use blue
    plt.subplot(122)
    plt.plot(primes, color='blue')
    plt.xlabel("primes")

    # this is just some boilderplate stuff that shows the plot
    # and saves it to a file on your computer
    plt.show()
    plt.savefig("collatz.png")
    plt.close()


if __name__ == '__main__':
    # setup variables

    # empty list to hold all the numbers that we get
    list_of_numbers = []

    # setting the largest number to 0 so we can something to use
    largest_number = 0

    # empty list to store our prime numbers
    list_of_primes = []

    # initializing steps variable to zero
    steps = 0

    # get user input and cast to int
    number = int(input('enter a number: ')) 

    #while we aren't at 1
    while number != 1:

        # take the current number and add it to our list
        list_of_numbers.append(int(number))

        #pass the current number to the check_largest function to see
        # if we have a new largest number and assign it to the variable
        largest_number = check_largest(largest_number, int(number))

        #increment our steps variable showing that we have done one check
        steps += 1

        # send the numbner to our is_prime method to see if it is indeed prime
        if is_prime(int(number)):
            # if is_prime returns True add it to the list
            # otherwise move on
            list_of_primes.append(int(number))

        # check if the number is even
        if number % 2 == 0:
            # if it is divide by 2
            number = number / 2
        else:
            # otherwise multiply by 3 and add 1
            number = number * 3 + 1


    # print out our results
    print()
    # create a dashed line. WHen you * by a number it repeasts the item 
    # in quotes that many times. 
    print('----'*20)
    # when you put a f before the quote it turns the string into a
    # "f string". That means that whatever variable you put betowee the { } will
    # be evaluated and outputed. So each of the lists wiill be displayed. 
    print(f"largest number is {largest_number:.0f}")
    print(f"primes found are  {list_of_primes }")
    print(f"number of steps to get to 1:  {steps}")
    plot_numbers(list_of_numbers,list_of_primes)


   

